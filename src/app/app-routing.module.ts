import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { BalanceComponent } from './bank/balance/balance.component';
import { DepositComponent } from './bank/deposit/deposit.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { FundTransferComponent } from './bank/fund-transfer/fund-transfer.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './bank/profile/profile.component';
import { StatementComponent } from './bank/statement/statement.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserComponent } from './user/user.component';
import { WithdrawComponent } from './bank/withdraw/withdraw.component';
import { BankComponent } from './bank/bank.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {path:'', redirectTo:'/user/login', pathMatch:'full'},
  {
    path:'user', component:UserComponent,
    children:[
      {path:'login', component:LoginComponent},
      {path:'register', component:RegistrationComponent}
    ]
  },
  {path:'home', component:HomeComponent},
  {path:'forbidden', component:ForbiddenComponent},
  {path:'adminpanel', component:AdminPanelComponent},
  {
    path:'bank', component:BankComponent,// canActivate:[AuthGuard],
    children:[
      {path:'profile', component:ProfileComponent},
      {path:'deposit', component:DepositComponent},
      {path:'withdraw', component:WithdrawComponent},
      {path:'transfer', component:FundTransferComponent},
      {path:'balance', component: BalanceComponent},
      {path:'statement', component:StatementComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ProfileComponent, DepositComponent, WithdrawComponent, FundTransferComponent, BalanceComponent, StatementComponent];
export const userRouting = [LoginComponent, RegistrationComponent];
