import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import { Client } from 'src/models/Client';

@Component({
  selector: 'app-fund-transfer',
  templateUrl: './fund-transfer.component.html',
  styles: [
  ]
})
export class FundTransferComponent implements OnInit {

  client: Client;
  constructor(private service : UserService,
    private Router: Router) { 
    
    this.client = 
    {
      "clientId": 3,
      "username": "starjohnson",
      "firstName": "Star",
      "lastName": "Johnson",
      "address": "Space",
      "age": 55,
      "emailAddress": "starjohnson@galcticprotectorate.com",
      "contactNumber": "5015558888",
      "accList": [
          {
              "accountNumber": "132548101",
              "balance": 5005.02,
              "transList": [
                  {
                      "transactionId": 4,
                      "description": "Transfer of money from savings to checking",
                      "debitAmount": 5000.02,
                      "creditAmount": 0.0,
                      "transactionDateTime": "2021-01-20T23:37:26.000+00:00"
                  }
              ],
              "tholder": {
                  "typeName": "checking",
                  "typeId": 1
              },
              "bholder": {
                  "branchName": "Santa Clara",
                  "branchId": 1
              }
          },
          {
              "accountNumber": "132548102",
              "balance": 4.999499998E7,
              "transList": [
                  {
                      "transactionId": 1,
                      "description": "Payday",
                      "debitAmount": 0.0,
                      "creditAmount": 200.0,
                      "transactionDateTime": "2021-01-20T23:37:22.000+00:00"
                  },
                  {
                      "transactionId": 2,
                      "description": "Purchase of Amazing Figure",
                      "debitAmount": 280.0,
                      "creditAmount": 0.0,
                      "transactionDateTime": "2021-01-20T23:37:22.000+00:00"
                  },
                  {
                      "transactionId": 3,
                      "description": "Transfer of money from savings to checking",
                      "debitAmount": -5000.02,
                      "creditAmount": 0.0,
                      "transactionDateTime": "2021-01-20T23:37:25.000+00:00"
                  }
              ],
              "tholder": {
                  "typeName": "savings",
                  "typeId": 2
              },
              "bholder": {
                  "branchName": "San Francisco",
                  "branchId": 3
              }
          }
      ]
  };
    

  }

  formModel = new FormGroup({
      amount:new FormControl(0)
    
  });
  onClick(amount:FormGroup) {
    let elementsFrom = <HTMLInputElement>document.getElementById('transferFrom');
    let accNumFrom = elementsFrom?.value;
    //console.log(accNumFrom);
    let elementsTo = <HTMLInputElement>document.getElementById('transferTo');
    let accNumTo = elementsTo?.value;
    //console.log(accNumTo);
    let funds = amount.get('amount')?.value;
   //console.log(amount.get('amount')?.value)
   this.service.fundtransfer(accNumFrom, accNumTo, funds).subscribe((response: any) =>{
     
   });
  }
  ngOnInit(): void {
  }

}
