import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb:FormBuilder, private http:HttpClient) { }
  readonly BaseURI = 'http://localhost:9030/api'

  formModel = this.fb.group({
    firstName:['', Validators.required],
    lastName:[''],
    address:[''],
    age:[''],
    username:['' ,Validators.required],
    password:['', [Validators.required, Validators.minLength(6)]],
    email:['', Validators.email],
    accountType:[''],
    branch:['']
  })

  fundtransfer(acc1:String, acc2:String, amount:number):Observable<Object>
  {
    var body = {
      acc1,
      acc2,
      amount
    }
    console.log(amount);
    return this.http.post(this.BaseURI + '/bank/transfer', body)
    }
  

  register(){
    var body = {
      firstName:this.formModel.value.firstName,
      lastName:this.formModel.value.lastName,
      address:this.formModel.value.address,
      age:this.formModel.value.age,
      username:this.formModel.value.username,
      password:this.formModel.value.password,
      email:this.formModel.value.email,
      accountType:this.formModel.value.accountType,
      branch:this.formModel.value.branch
    };
    console.log(body)
    return this.http.post(this.BaseURI+'/users/register', body);
  }

  login(formData:any){
    console.log(formData);
    return this.http.post(this.BaseURI+'/users/login', formData);
  }
}
