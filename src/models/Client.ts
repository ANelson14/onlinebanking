import {Account} from 'src/models/Account'
export interface Client {
	clientId:number;
	address:string;
	age:number;
	contactNumber:string;
	emailAddress:string;
	firstName:string;
	lastName:string;
	username:string;
	accList: Account[];
}